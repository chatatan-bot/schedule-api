# Chatatan Schedule API

[![pipeline status](https://gitlab.com/chatatan-bot/schedule-api/badges/master/pipeline.svg)](https://gitlab.com/chatatan-bot/schedule-api/-/commits/master)
[![coverage report](https://gitlab.com/chatatan-bot/schedule-api/badges/master/coverage.svg)](https://gitlab.com/chatatan-bot/schedule-api/-/commits/master)

## About Chatatan

Chatatan is a LINE bot application that serves as a personal notes and schedule keeper. Check out our Chatatan LINE bot!

<a href="https://line.me/R/ti/p/%40045nhzyf"><img height="36" alt="Add Chatatan" src="https://scdn.line-apps.com/n/line_add_friends/btn/en.png"></a>

## Features

Chatatan Schedule API is one of the four services in Chatatan. It acts as the API endpoint to retrieve data from Chatatan's Schedule Feature.

|#|Path|Method|Return Type|Desc|
|---|:---|---|---|---|
|1| `event`| **POST** |`void`| Add event to database in the form of json object, which consists of `userId`, `name`, `startTime`, `endTime`, and `isRoutine`. In the case of `date`, it is optional as users may only input `dayName`. |
|2| `event`| **PUT** |`List<Event>`| Edit data of either `startTime`, `endTime`, or `date` of an event. | 
|3| `event/{id}`| **DELETE** |`String`| Delete an event based on the generated id, and provide String response if the event has been successfully deleted or if the event is not found. |
|4| `follow/{userId}`| **POST** |`void`| Initialize empty schedule for a user. |
|5| `{week}/{userId}`| **GET** |`List<Event>`| Get all events from a user based on the choice of weeks: `this` for this week, `next` for next week, or `routine` for routine (weekly) events. |
|6| `details/{week}/{userId}`| **GET** |`String`| Get the details of all events from point 5 in the form of String. |
|7| `search/{date}/{userId}`| **GET** |`List<Event>`| Get all events from a user based on a specific date. |
|8| `details/search/{date}/{userId}`| **GET** |`String`| Get the details of the events from point 7 in the form of String. |
|9| `update/{userId}`| **POST** |`void`| Update a user's schedule if it is outdated based on the user's last access. |
|10| `/`| **GET** |`String`| Display default content of Chatatan's Schedule API when accessed through url. |

## Authors [B10]
* [Wulan Mantiri](https://gitlab.com/wulanmantiri_)
* Naufal Hilmi Irfandi
* Bayukanta Iqbal Gunawan
* Gilbert Stefano Wijaya
* Mohammad Hasan Albar

## Acknowledgements
* CS UI - Advanced Programming B 2020