package com.restapi.schedule.repository;

import com.restapi.schedule.core.Day;
import java.time.DayOfWeek;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ScheduleRepository extends JpaRepository<Day, Long> {

    List<Day> findByUserId(String userId);

    Day findDistinctDayByUserIdAndData(String userId, DayOfWeek data);
    
}
