package com.restapi.schedule.repository;

import com.restapi.schedule.core.Event;
import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EventRepository extends JpaRepository<Event, Long> {

    Event findById(long id);

    List<Event> findByUserIdAndIsRoutine(String userId, boolean isRoutine);

    List<Event> findByUserIdAndDateAndIsRoutine(String userId, LocalDate date, boolean isRoutine);

    List<Event> findByUserIdAndDayNameAndIsRoutine(String userId, String day, boolean isRoutine);

}
