package com.restapi.schedule.controller;

import com.restapi.schedule.core.Day;
import com.restapi.schedule.core.Event;
import com.restapi.schedule.core.User;
import com.restapi.schedule.repository.EventRepository;
import com.restapi.schedule.repository.ScheduleRepository;
import com.restapi.schedule.repository.UserRepository;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ScheduleController {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/")
    public String landing() {
        return "Hi, I am Chatatan's Schedule API";
    }

    @PostMapping("/follow/{userId}")
    public void initializeSchedule(@PathVariable(value = "userId") String userId) {
        if (userRepository.findByUserId(userId) == null) {
            User user = new User(userId);
            userRepository.save(user);

            for (DayOfWeek dayOfWeek: DayOfWeek.values()) {
                Day day = new Day(userId, dayOfWeek);
                scheduleRepository.save(day);
            }
        }
    }

    @GetMapping("/details/{week}/{userId}")
    public String getScheduleDetails(@PathVariable(value = "week") String week,
        @PathVariable(value = "userId") String userId) {
        List<Day> schedule = getSchedule(userId, week);
        StringBuilder output = new StringBuilder();
        for (Day day: schedule) {
            if (day.getListOfEvents().size() != 0) {
                output.append(day.getDetails());
            }
        }
        return output.toString();
    }

    private List<Day> getThisWeeksSchedule(String userId) {
        return scheduleRepository.findByUserId(userId); 
    }

    private List<Day> getSchedule(String userId, String week) {
        if (week.equals("this")) {
            return getThisWeeksSchedule(userId);
        } else {
            List<Day> schedule = createEmptyScheduleList(userId);
            for (Event event: getEventsByWeek(week, userId)) {
                String dayName = event.getDayName();
                int dayIndex = DayOfWeek.valueOf(dayName).getValue() - 1;
                Day day = schedule.get(dayIndex);
                day.add(event);
            }
            return schedule;
        }     
    }

    private List<Day> createEmptyScheduleList(String userId) {
        List<Day> schedule = new ArrayList<>();
        for (DayOfWeek dayOfWeek: DayOfWeek.values()) {
            schedule.add(new Day(userId, dayOfWeek));
        }
        return schedule;
    }

    @GetMapping("/{week}/{userId}")
    public List<Event> getEventsByWeek(@PathVariable(value = "week") String week,
        @PathVariable(value = "userId") String userId) {
        List<Event> events = eventRepository.findByUserIdAndIsRoutine(userId, true);
        List<Event> nonRepeatingEvents = eventRepository.findByUserIdAndIsRoutine(userId, false);

        if (week.equals("this")) {
            for (Event event: nonRepeatingEvents) {
                if (isWithinThisWeek(event)) {
                    events.add(event);
                }
            }
        } else if (week.equals("next")) {
            for (Event event: nonRepeatingEvents) {
                if (isWithinNextWeek(event)) {
                    events.add(event);
                }
            }
        }

        Collections.sort(events);
        return events;
    }

    @GetMapping("/search/{date}/{userId}")
    public List<Event> getEventsByDate(@PathVariable(value = "date") String dateString,
        @PathVariable(value = "userId") String userId) {
        LocalDate date = LocalDate.parse(dateString);
        String dayName = date.getDayOfWeek().toString();
        List<Event> searchedEvents;
        List<Event> routineEvents;
        searchedEvents = eventRepository.findByUserIdAndDateAndIsRoutine(userId, date, false);
        routineEvents = eventRepository.findByUserIdAndDayNameAndIsRoutine(userId, dayName, true);
        searchedEvents.addAll(routineEvents);
        return searchedEvents;
    }

    @GetMapping("/details/search/{date}/{userId}")
    public String getSearchDetails(@PathVariable(value = "date") String dateString,
        @PathVariable(value = "userId") String userId) {
        StringBuilder output = new StringBuilder();
        for (Event event: getEventsByDate(dateString, userId)) {
            output.append(event.getDetails());
        }
        return output.toString();
    }

    private Day getDay(String userId, String dayName) {
        DayOfWeek dayOfWeek = DayOfWeek.valueOf(dayName);
        return scheduleRepository.findDistinctDayByUserIdAndData(userId, dayOfWeek);
    }

    @PostMapping("/event")
    public void createEvent(@RequestBody Event event) throws Exception {
        String userId = event.getUserId();
        Day day = getDay(userId, event.getDayName());

        if (event.getDate().equals("")) {
            String date = getDateBasedOnDay(event);
            event.setDate(date);
            event.setDay(day);
        } else if (event.getIsRoutine()) {
            event.setDay(day);
        } else if (isWithinThisWeek(event)) {
            event.setDay(day);
        }
        eventRepository.save(event);
    }

    private String getDateBasedOnDay(Event event) {
        LocalDate currentDate = LocalDate.now();
        DayOfWeek today = currentDate.getDayOfWeek();
        DayOfWeek eventDay = DayOfWeek.valueOf(event.getDayName());
        int diff = eventDay.getValue() - today.getValue();
        return currentDate.plusDays(diff).toString();
    }

    private boolean isWithinThisWeek(@RequestBody Event event) {
        LocalDate dateOfEvent = LocalDate.parse(event.getDate());
        LocalDate today = LocalDate.now();
        return isWithinTheWeekOf(today, dateOfEvent);
    }

    private boolean isWithinNextWeek(@RequestBody Event event) {
        LocalDate dateOfEvent = LocalDate.parse(event.getDate());
        LocalDate nextWeek = LocalDate.now().plusWeeks(1);
        return isWithinTheWeekOf(nextWeek, dateOfEvent);
    }

    private boolean isWithinTheWeekOf(LocalDate referencedDate, LocalDate date) {
        int dayNumber = referencedDate.getDayOfWeek().getValue();
        long duration = ChronoUnit.DAYS.between(referencedDate, date) + dayNumber;
        return 1 <= duration && duration <= 7;
    }
    
    @PostMapping("/test-outdated-user")
    public void createDummyOutdatedUserForTestingPurposes(@RequestBody User user) {
        userRepository.save(user);
    }

    @PostMapping("/update/{userId}")
    public void updateSchedule(@PathVariable(value = "userId") String userId) {
        User user = userRepository.findByUserId(userId);
        LocalDate lastAccess = user.getLastAccess();
        LocalDate today = LocalDate.now();
        if (!isWithinTheWeekOf(today, lastAccess)) {
            renewSchedule(userId);
        }
        user.updateLastAccess();
    }

    @PostMapping("/renew/{userId}")
    public void renewSchedule(@PathVariable(value = "userId") String userId) {
        for (Day day: getThisWeeksSchedule(userId)) {
            for (Event event: day.getListOfEvents()) {
                if (!event.getIsRoutine()) {
                    deleteEvent(event.getId());
                }
            }
        }
        List<Event> nonRepeatingEvents = eventRepository.findByUserIdAndIsRoutine(userId, false);
        for (Event event: nonRepeatingEvents) {
            if (isWithinThisWeek(event)) {
                Day day = getDay(userId, event.getDayName());
                event.setDay(day);
            }
        }
    }

    @PutMapping("/event")
    public List<Event> editEvent(@RequestBody Event editedEvent) {
        List<Event> eventList = new ArrayList<>();
        try {
            Event event = eventRepository.findById(editedEvent.getId());
            event.setName(editedEvent.getName());
            event.setStartTime(editedEvent.getStartTime());
            event.setEndTime(editedEvent.getEndTime());
            event.setDayName(editedEvent.getDayName());
            event.setIsRoutine(editedEvent.getIsRoutine());
            event.setDate(editedEvent.getDate());
            createEvent(event);
            eventList.add(event);
            return eventList;
        } catch (Exception e) {
            return eventList;
        } 
    }

    @DeleteMapping("/event/{id}")
    public String deleteEvent(@PathVariable(value = "id") long id) {
        try {
            Event event = eventRepository.findById(id);
            eventRepository.delete(event);
            return String.format("Event: %s deleted!", event.getName());
        } catch (Exception e) {
            return "Worry not! The event has been previously deleted :)";
        }
    }

}
