package com.restapi.schedule.core;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "events")
public class Event implements TimeComponent, Comparable<Event> {
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "name")
    private String name;
    
    @Column(name = "start_time")
    private LocalTime startTime;

    @Column(name = "end_time")
    private LocalTime endTime;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "is_routine")
    private boolean isRoutine;

    @Column(name = "day_name")
    private String dayName;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "day", nullable = true)
    @JsonIgnore
    private Day day;

    public Event(String userId, String name, LocalTime startTime,
        LocalTime endTime, String dayName, boolean isRoutine) {
        this.userId = userId;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.isRoutine = isRoutine;
        this.dayName = dayName;
    }

    public Event(String userId, String name, LocalTime startTime,
        LocalTime endTime, LocalDate date, boolean isRoutine) {
        this.userId = userId;
        this.name = name;
        this.startTime = startTime;
        this.endTime = endTime;
        this.isRoutine = isRoutine;
        this.date = date;
        this.dayName = date.getDayOfWeek().toString();
    }

    public Event() {
        super();
    }

    public String getDetails() {
        return String.format("%s-%s\n%s\n", this.startTime, this.endTime, this.name);
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartTime() {
        return this.startTime.toString();
    }

    public void setStartTime(String timeString) throws Exception {
        this.startTime = LocalTime.parse(timeString);
    }

    public String getEndTime() {
        return this.endTime.toString();
    }

    public void setEndTime(String timeString) throws Exception {
        this.endTime = LocalTime.parse(timeString);
    }

    public String getDate() {
        return this.date != null ? this.date.toString() : "";
    }

    public void setDate(String dateString) throws Exception {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
        LocalDate date = LocalDate.parse(dateString, formatter);
        this.date = date;
        this.dayName = date.getDayOfWeek().toString();
    }

    public boolean getIsRoutine() {
        return this.isRoutine;
    }

    public void setIsRoutine(boolean isRoutine) {
        this.isRoutine = isRoutine;
    }

    public String getDayName() {
        return this.dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public Day getDay() {
        return this.day;
    }

    public void setDay(Day day) {
        this.day = day;
        this.dayName = day.getName();
    }

    public int compareTo(Event other) {
        int thisDayNumber = DayOfWeek.valueOf(this.dayName).getValue();
        int otherDayNumber = DayOfWeek.valueOf(other.dayName).getValue();
        if (thisDayNumber - otherDayNumber == 0) {
            return this.startTime.compareTo(other.startTime);
        } else {
            return thisDayNumber - otherDayNumber;
        }
    }

}