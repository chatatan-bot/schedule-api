package com.restapi.schedule.core;

public interface TimeComponent {
    String getDetails();
}