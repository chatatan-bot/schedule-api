package com.restapi.schedule.core;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "days")
public class Day implements TimeComponent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "data")
    private DayOfWeek data;

    @OneToMany(mappedBy = "day", cascade = CascadeType.ALL)
    private List<Event> events = new ArrayList<>();

    public Day(String userId, DayOfWeek dayOfWeek) {
        this.userId = userId;
        this.data = dayOfWeek;
    }

    public Day() {
        super();
    }

    public void add(Event event) {
        events.add(event);
        Collections.sort(events);
    }

    public void remove(Event event) {
        events.remove(event);
    }

    public String getDetails() {
        StringBuilder output = new StringBuilder(this.data.toString());
        output.append("\n");
        for (Event event : events) {
            output.append(event.getDetails());
        }
        return output.toString();
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return this.userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return this.data.toString();
    }

    public void setName(String name) {
        this.data = DayOfWeek.valueOf(name);
    }

    public DayOfWeek getData() {
        return this.data;
    }

    public List<Event> getListOfEvents() {
        return this.events;
    }
}
