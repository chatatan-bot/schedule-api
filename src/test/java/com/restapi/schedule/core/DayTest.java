package com.restapi.schedule.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.DayOfWeek;
import java.time.LocalTime;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class DayTest {

    Day day;
    Day day1;
    Event event;

    @Before
    public void setUp() throws Exception {
        day = new Day();
        day.setUserId("1");
        day.setName("MONDAY");
        day.setId(1);

        day1 = new Day("2", DayOfWeek.THURSDAY);

        event = new Event("2", "TDD Event", LocalTime.of(12,00), 
            LocalTime.of(13,45), "MONDAY", false);
    }

    @Test
    public void testGetId() {
        assertEquals(day.getId(), 1);
    }

    @Test
    public void testGetUserId() {
        assertEquals(day.getUserId(), "1");
        assertEquals(day1.getUserId(), "2");
    }

    @Test
    public void testGetName() {
        assertEquals(day.getName(), "MONDAY");
        assertEquals(day1.getName(), "THURSDAY");
    }

    @Test
    public void testGetData() {
        assertEquals(day.getData(), DayOfWeek.MONDAY);
        assertEquals(day1.getData(), DayOfWeek.THURSDAY);
    }

    @Test
    public void testGetDetails() {
        day.add(event);
        assertEquals(day.getDetails(), "MONDAY\n12:00-13:45\nTDD Event\n");
    }

    @Test
    public void testAddEvent() {
        day.add(event);
        assertEquals(day.getListOfEvents().size(), 1);
    }

    @Test
    public void testRemovingNonexistingEventHasNoError() {
        day.remove(event);
        assertEquals(day.getListOfEvents().size(), 0);
    }

    @Test
    public void testRemoveExistingEvent() {
        day.add(event);
        assertEquals(day.getListOfEvents().size(), 1);
        day.remove(event);
        assertEquals(day.getListOfEvents().size(), 0);
    }
}

