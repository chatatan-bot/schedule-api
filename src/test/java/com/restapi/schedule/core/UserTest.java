package com.restapi.schedule.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class UserTest {

    User user;
    User user1;

    @Before
    public void setUp() throws Exception {
        user = new User();
        user.setUserId("1806205666");
        user.updateLastAccess();

        user1 = new User("1");
        user1.setLastAccess(LocalDate.of(2020, 5, 8));
    }

    @Test
    public void testGetUserId() {
        assertEquals(user.getUserId(), "1806205666");
        assertEquals(user1.getUserId(), "1");
    }

    @Test
    public void testGetLastAccess() {
        assertEquals(user.getLastAccess(), LocalDate.now());
        assertEquals(user1.getLastAccess(), LocalDate.of(2020, 5, 8));
    }
}
