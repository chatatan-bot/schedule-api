package com.restapi.schedule.core;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class EventTest {

    Event event;
    Event event1;
    Event event2;
    Day day;

    @Before
    public void setUp() throws Exception {
        event = new Event();
        event.setId(1);
        event.setUserId("1");
        event.setName("Demo TK Adprog");
        event.setStartTime("15:00");
        event.setEndTime("17:40");
        event.setDate("2020-04-23");
        event.setIsRoutine(true);

        day = new Day("1", DayOfWeek.THURSDAY);
        event.setDay(day);

        event1 = new Event("2", "TDD Event", LocalTime.of(12,00), 
            LocalTime.of(13,45), "MONDAY", false);

        event2 = new Event("1", "Applying Clean Code", LocalTime.of(12,00), 
            LocalTime.of(13,45), LocalDate.of(2020, 4, 20), false);
    }

    @Test
    public void testGetId() {
        assertEquals(event.getId(), 1);
    }

    @Test
    public void testGetUserId() {
        assertEquals(event.getUserId(), "1");
        assertEquals(event1.getUserId(), "2");
    }

    @Test
    public void testGetName() {
        assertEquals(event.getName(), "Demo TK Adprog");
        assertEquals(event1.getName(), "TDD Event");
    }

    @Test
    public void testGetStartTime() {
        assertEquals(event.getStartTime(), "15:00");
        assertEquals(event1.getStartTime(), "12:00");
    }

    @Test
    public void testGetEndTime() {
        assertEquals(event.getEndTime(), "17:40");
        assertEquals(event1.getEndTime(), "13:45");
    }

    @Test
    public void testGetDate() {
        assertEquals(event.getDate(), "2020-04-23");
        assertEquals(event1.getDate(), "");
    }

    @Test
    public void testGetDetails() {
        assertEquals(event.getDetails(), "15:00-17:40\nDemo TK Adprog\n");
        assertEquals(event1.getDetails(), "12:00-13:45\nTDD Event\n");
    }

    @Test
    public void testGetDayName() {
        assertEquals(event.getDayName(), "THURSDAY");
        assertEquals(event1.getDayName(), "MONDAY");
    }

    @Test
    public void testGetDay() {
        assertEquals(event.getDay().getName(), day.getName());
    }

    @Test
    public void testGetIsRoutine() {
        assertEquals(event.getIsRoutine(), true);
        assertEquals(event1.getIsRoutine(), false);
    }

    @Test
    public void testCompareTo() {
        assertEquals(event.compareTo(event1), 3);
        assertEquals(event1.compareTo(event2), 0);
    }
}

