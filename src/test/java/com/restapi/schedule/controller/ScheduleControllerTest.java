package com.restapi.schedule.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.restapi.schedule.utilities.JsonCreator;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
public class ScheduleControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MockMvc mockMvc1;

    String userId;
    String eventJsonInFarFuture;
    String eventJsonWithinThisWeek;
    String eventJsonInThePast;
    String eventJsonWithoutDate;
    
    @Before 
    public void setUp() throws Exception {
        userId = "1806205666";

        String inputValues = "TDD Integration with Line Bot\n00:15\n14:30\n2020-07-05\nno";
        JSONObject json = JsonCreator.createEvent(userId, inputValues);
        eventJsonInFarFuture = json.toString();

        inputValues = "TDD Schedule Controller\n00:15\n14:30\n2020-06-25\nno";
        json = JsonCreator.createEvent(userId, inputValues);
        eventJsonWithinThisWeek = json.toString();

        inputValues = "TDD Json Creator\n00:15\n14:30\n2020-04-23\nno";
        json = JsonCreator.createEvent(userId, inputValues);
        eventJsonInThePast = json.toString();

        inputValues = "Demo TK Adprog\n15:00\n17:40\nthursday\nyes";
        json = JsonCreator.createEvent(userId, inputValues);
        eventJsonWithoutDate = json.toString();
    }

    @Test
    public void testLandingPage() throws Exception {
        String content = mockMvc1.perform(get("/"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(content, "Hi, I am Chatatan's Schedule API");
    }

    @Test
    public void testInitializeSchedule() throws Exception {
        mockMvc.perform(post("/follow/1")).andExpect(status().isOk());
    }

    @Test
    public void testInitialScheduleHasNoEvents() throws Exception {
        mockMvc1.perform(post("/follow/18062056"));

        String content = mockMvc1.perform(get("/details/this/18062056"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(content, "");
    }

    @Test
    public void testCreateEventThenGetScheduleDetails() throws Exception {
        mockMvc.perform(post("/follow/1806205666"));

        mockMvc.perform(post("/event")
                .content(eventJsonWithinThisWeek)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post("/event")
                .content(eventJsonWithoutDate)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        
        String content = mockMvc.perform(get("/details/this/1806205666"))
                    .andExpect(status().isOk())
                    .andReturn().getResponse().getContentAsString();
        String output = "";
        output = "THURSDAY\n00:15-14:30\nTDD Schedule Controller\n15:00-17:40\nDemo TK Adprog\n";
        assertEquals(content, output);

        content = mockMvc.perform(get("/details/routine/1806205666"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(content, "THURSDAY\n15:00-17:40\nDemo TK Adprog\n");
        
        mockMvc.perform(get("/this/1806205666")).andExpect(status().isOk());
        mockMvc.perform(get("/next/1806205666")).andExpect(status().isOk());
    }

    @Test
    public void testCreateFutureEvents() throws Exception {
        mockMvc.perform(post("/follow/1806205666"));

        mockMvc.perform(post("/event")
                .content(eventJsonInFarFuture)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        String inputValues = "Semester 4 finished\n00:15\n14:30\n2020-08-17\nno";
        JSONObject json = JsonCreator.createEvent(userId, inputValues);
        json.put("id", 20);
        String eventFarFarAway = json.toString();

        mockMvc.perform(post("/event")
                .content(eventFarFarAway)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
        
        inputValues = "Semester finished\n00:15\n14:30\n2020-07-26\nno";
        json = JsonCreator.createEvent(userId, inputValues);
        json.put("id", 20);
        String editedEvent = json.toString();

        mockMvc.perform(put("/event")
                .content(editedEvent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        String content = mockMvc.perform(get("/details/next/1806205666"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        String output = "THURSDAY\n15:00-17:40\nDemo TK Adprog\n";
        output += "SUNDAY\n00:15-14:30\nTDD Integration with Line Bot\n";
        assertEquals(content, output);
        
        content = mockMvc.perform(get("/details/search/2020-07-05/1806205666"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(content, "00:15-14:30\nTDD Integration with Line Bot\n");
    }

    @Test
    public void testSearchEvent() throws Exception {
        String content = mockMvc.perform(get("/details/search/2020-05-16/1806205666"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(content, "");
    }

    @Test
    public void testUpdateOutdatedUser() throws Exception {
        String user = "{\"userId\": \"20210101\", \"lastAccess\":\"2020-04-23\"}";

        mockMvc.perform(post("/test-outdated-user")
                .content(user)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mockMvc.perform(post("/update/20210101")).andExpect(status().isOk());
    }

    @Test
    public void testUpdateAndRenewSchedule() throws Exception {
        mockMvc.perform(post("/follow/180620566"));

        String inputValues = "TDD Delete and Update\n14:00\n16:30\n2020-06-22\nyes";
        JSONObject json = JsonCreator.createEvent("180620566", inputValues);
        String event = json.toString();

        mockMvc.perform(post("/event")
                .content(event)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        inputValues = "TDD Delete and Update\n14:00\n16:30\nthursday\nno";
        json = JsonCreator.createEvent("180620566", inputValues);
        event = json.toString();
        
        mockMvc.perform(post("/event")
                .content(event)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        inputValues = "UAS\n14:00\n16:30\n2020-07-02\nno";
        json = JsonCreator.createEvent("180620566", inputValues);
        event = json.toString();
        
        mockMvc.perform(post("/event")
                .content(event)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
                
        mockMvc.perform(post("/update/180620566")).andExpect(status().isOk());
        mockMvc.perform(post("/renew/180620566")).andExpect(status().isOk());

        mockMvc.perform(get("/this/180620566")).andExpect(status().isOk());
        mockMvc.perform(get("/next/180620566")).andExpect(status().isOk());
    }

    @Test
    public void testDeleteNonexistingEvent() throws Exception {
        String content = mockMvc.perform(delete("/event/0"))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(content, "Worry not! The event has been previously deleted :)");
    }
    
    @Test
    public void testEditNonexistingEvent() throws Exception {
        JSONObject json = new JSONObject();
        json.put("id", 0);
        String editedEvent = json.toString();
        
        String content = mockMvc.perform(put("/event")
                .content(editedEvent)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertEquals(content, "[]");
    }
        
}
