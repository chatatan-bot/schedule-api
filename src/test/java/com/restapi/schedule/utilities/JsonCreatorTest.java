package com.restapi.schedule.utilities;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest()
public class JsonCreatorTest {

    JsonCreator jsonCreator;
    JSONObject eventJsonWithDate;
    JSONObject eventJsonWithDayNameOnly;
    String userId;

    @Before
    public void setUp() {
        userId = "1806205666";
        jsonCreator = new JsonCreator();

        eventJsonWithDate = new JSONObject();
        eventJsonWithDate.put("userId", userId);
        eventJsonWithDate.put("name", "TDD Json Creator");
        eventJsonWithDate.put("startTime", "20:30");
        eventJsonWithDate.put("endTime", "23:30");
        eventJsonWithDate.put("isRoutine", false);
        eventJsonWithDate.put("date", "2020-04-23");
        eventJsonWithDate.put("dayName", "THURSDAY");

        eventJsonWithDayNameOnly = new JSONObject();
        eventJsonWithDayNameOnly.put("userId", userId);
        eventJsonWithDayNameOnly.put("name", "Demo TK Adprog");
        eventJsonWithDayNameOnly.put("startTime", "15:00");
        eventJsonWithDayNameOnly.put("endTime", "17:40");
        eventJsonWithDayNameOnly.put("isRoutine", true);
        eventJsonWithDayNameOnly.put("dayName", "THURSDAY");
    }

    @Test
    public void testCreateDay() {
        JSONObject expectedJson = new JSONObject();
        expectedJson.put("userId", userId);
        expectedJson.put("name", "MONDAY");
        JSONAssert.assertEquals(JsonCreator.createDay(userId, "MONDAY"), expectedJson, false);
    }

    @Test
    public void testCreateEventAcceptDateInput() {
        String inputValues = "TDD Json Creator\n20:30\n23:30\n2020-04-23\nno";
        JSONObject result = JsonCreator.createEvent(userId, inputValues);
        JSONAssert.assertEquals(result, eventJsonWithDate, false);
    }

    @Test
    public void testCreateEventAcceptDayNameInput() {
        String inputValues = "Demo TK Adprog\n15:00\n17:40\nthursday\nyes";
        JSONObject result = JsonCreator.createEvent(userId, inputValues);
        JSONAssert.assertEquals(result, eventJsonWithDayNameOnly, false);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIncorrectStartTimeFormat() {
        String inputValues = "Demo TK Adprog\n1a:a0\n17:40\nthursday\nyes";
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIncorrectEndTimeFormat() {
        String inputValues = "Demo TK Adprog\n15:00\n1a:akd0\nthursday\nyes";
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIllogicalTimeInput() {
        String inputValues = "Demo TK Adprog\n17:40\n15:00\nthursday\nyes";
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIncorrectDateFormat() {
        String inputValues = "TDD Json Creator\n20:30\n23:30\n2020-4-23\nno";
        JsonCreator.createEvent(userId, inputValues);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateEventWithIncorrectIsRoutineFormat() {
        String inputValues = "TDD Json Creator\n20:30\n23:30\n2020-4-23\nBUKAN ROUTINE WOI";
        JsonCreator.createEvent(userId, inputValues);
    }
}

